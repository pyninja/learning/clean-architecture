# Domain: Room
from __future__ import annotations
import uuid
import dataclasses


@dataclasses.dataclass
class Room:
    code: uuid.UUID
    size: int
    price: int
    longitude: float
    latitude: float

    @classmethod
    def from_dict(cls, data: dict) -> Room:
        return cls(**data)

    def to_dict(self):
        return dataclasses.asdict(self)

