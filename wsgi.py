from dotenv import load_dotenv
from app.factory import create_app
import os

_ = load_dotenv()

app = create_app(os.environ["FLASK_CONFIG"])
