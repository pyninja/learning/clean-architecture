import pytest
from app.factory import create_app


@pytest.fixture
def app():
    app = create_app("testing")
    return app
